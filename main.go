package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	dbHost := "host.docker.internal"
	dbPort := "3306"
	dbUser := "root"
	dbPass := "GavindaDB"
	dbName := "article"
	connection := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUser, dbPass, dbHost, dbPort, dbName)
	val := url.Values{}
	val.Add("parseTime", "1")
	val.Add("loc", "Asia/Jakarta")
	dsn := fmt.Sprintf("%s?%s", connection, val.Encode())
	fmt.Println("Opening Mysql connection using dsn : ", dsn)
	_, err := sql.Open(`mysql`, dsn)
	if err != nil {
		panic(err)
	}
	http.HandleFunc("/", home)
	log.Fatal(http.ListenAndServe(":7575", nil))
}

func home(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	type resp struct {
		Message string `json:"message"`
	}
	var p resp
	p.Message = "hello"
	json.NewEncoder(w).Encode(p)
}
