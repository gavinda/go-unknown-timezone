# Simple app to reproduce error 'unknown time zone' in golang

## Step to reproduce error

1. Build as docker image with base image [golang:1.13.0-alpine](https://hub.docker.com/layers/golang/library/golang/1.13.0-alpine/images/sha256-14ab699c7afd48a54598a4764dddd30e2b232025bc16b165d822e0d175a18615?context=explore)
2. Run the image. It will build this app to binary.
3. Move the binary to other container with base image [alpine](https://hub.docker.com/layers/alpine/library/alpine/latest/images/sha256-4d5c5951669588e23881c158629ae6bac4ab44866d5b4d150c3f15d91f26682b?context=explore)
4. Check the container logs. It will show the panic :
```
panic: unknown time zone Asia/Jakarta
```

## Usage

1. Set your mysql database in `main.go`
2. Just run `make start` let the machine prepare what you need
3. See the logs with `docker logs error_tz_container` and you will see the error above
4. Run `make stop` to delete all container and image