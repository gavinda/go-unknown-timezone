start:
	docker build -t error_tz:1.0 . ; docker run  --name error_tz_container -d -p 7575:7575 error_tz:1.0 
stop:
	docker rm error_tz_container -f ; docker rmi error_tz:1.0 ; docker image prune -f
