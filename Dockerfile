FROM golang:1.13.0-alpine as builder
COPY . /home/app/
WORKDIR /home/app/
RUN go mod download
RUN go build -o tz_app
EXPOSE 7575


# Create final image
FROM alpine
WORKDIR /home
COPY --from=builder /home/app/tz_app .
EXPOSE 6060
CMD ["./tz_app"]